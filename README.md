# Gp2 To Gp3 Script  
Converts gp2 to gp3 Volume Type.
### Install AWS CLI (for Linux)
#### Prerequisites
* You must be able to extract or "unzip" the downloaded package. Most of the operating system already has it. 
* Check unzip installation by ```unzip -v```. It will return the version installed - ```UnZip 6.00 of 20 April 2009, by Debian. Original by Info-ZIP```
* If it does not returns the version installed or throws an error try installing by running - ```sudo apt install unzip``` . And check installation by - ```unzip -v```.

**Open Terminal and execute**  
Installs AWS CLI to the system.  
- ```curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" unzip awscliv2.zip```  
- ```sudo ./aws/install```  

**check Installation by:**  

``` aws --version``` will return version installed -  
```aws-cli/2.1.29 Python/3.8.8 Linux/4.15.0-136-generic exe/x86_64.ubuntu.16 prompt/off```
  
**Configure aws**  
After installing AWS CLI, It has to be configured with the required Credentials so as to connect to aws services. Open Terminal and execute -
>``` aws configure ```   

provide Asses Key ID, Secret Asses key and set default region name

**Steps involved in executing scripts**   
Scripts expects two arguments:  
``` -r ``` - (optional) Specific Regions Code whose all Volume types to be converted to gp3 (space separated). If not provided it will convert all regions' volume type.  
```-p``` - (optional) Path to the text file containing Specefic EBS Volumes ids which is not to be converted to gp3. If not provided all EBS Volumes will be converted to gp3. 


**From terminal run**  
>```bash convert_gp2.sh -r "region-code1 region-code2" -p path-to-text-file```  

Note every EBS volume ids must be in newline in path-to-text-file.txt  
**Format of the txt file-**  
>`vol-id1`  
`vol-id2`  
`vol-id3`  
 
#### Sample examples -
- On executing ```bash convert_gp2.sh -r "ap-southeast-1 us-west-2" -p path-to-text-file``` will convert all gp2 volume types at ```ap-southeast-1``` and ```us-west-2``` regions to gp3, excluding all volumes which are in ```path-to-text-file```. 
-  On executing ```bash convert_gp2.sh -p path-to-text-file``` will convert all gp2 volume types of all regions' to gp3, excluding all volumes which are in ```path-to-text-file```
-  On executing ```bash convert_gp2.sh ``` will convert all gp2 volume types of all regions' to gp3 and none of the volumes will be excluded.

