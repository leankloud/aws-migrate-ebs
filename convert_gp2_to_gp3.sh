#! /bin/bash 

convert_to_gp3(){
  local -n vol_id=$1
  local -n region_name=$2
  echo "modifying vol-id: $vol_id - GP2 to GP3" 
  # converts to gp3 
  echo $(aws ec2 modify-volume --region $region_name --volume-id $vol_id --volume-type gp3)
  echo "modified vol-id: $vol_id - to GP3 successfully" 
}


# Parsing Arguments
while getopts p:r: fl
do
  case "${fl}" in
     r) passed_regions=$OPTARG;;
     p) blacklist_path=${OPTARG};;
  esac
done

# gets volume ids from passed txt file 
if [[ -s "$blacklist_path" ]]; then
    black_list=$(<$blacklist_path)
else 
    echo "All volumes will get converted to GP3"
    black_list=()
fi

if [[ -z $passed_regions ]]; then 
    echo "Volumes from all regions will be converted to GP3***"
    list_of_regions=$(aws ec2 describe-regions --no-all-regions --query Regions[*].RegionName --output text)
else 
    list_of_regions=(${passed_regions})
fi

for region in ${list_of_regions[*]}; do
    vol_ids=$(aws ec2 describe-volumes --region $region --query Volumes[*].VolumeId --output text)
    if [[ "${vol_ids}" != '' ]]; then
      echo
      echo "Modifying EBS volumes in Region: $region"
      echo
      for volume_id in ${vol_ids[@]} ; do
        if [[ ! "${black_list[@]}" =~ "$volume_id" ]]; then
          # gets the volume type 
          volume_type=$(aws ec2 describe-volumes --region $region --filter Name=volume-id,Values=$volume_id --query Volumes[*].VolumeType --output text)
          if [ "$volume_type" == "gp2" ]; then
            convert_to_gp3 volume_id region
          else
            echo "Ignoring $vol_id with volume type $volume_type"
          fi
        fi
      done
    fi   
done  

